#!/usr/bin/python
# Rocrail XML script example: Power ON.
 
from socket import *
 
# Subroutine for adding the XML-Header and send it to the server
def sendMsg( s, xmlType, xmlMsg ):
  s.send("<xmlh><xml size=\"%d\" name=\"%s\"/></xmlh>%s" %(len(xmlMsg), xmlType, xmlMsg))
 
# Create the server connection
s = socket(AF_INET, SOCK_STREAM)
s.connect(('localhost', 8051))
 
# Compose the power on command and send it
# rrMsg = "<sys cmd=\"go\"/>"
# sendMsg( s, "sys", rrMsg )
rrMsg = "<sw id=\"sw-abp-w\" cmd=\"turnout\"/>"
sendMsg( s, "sw", rrMsg )

	# $rrMsg = '<sw id="sw1" cmd="turnout"/>';
	# send($fp, 'sw', $rrMsg);	
 
# Close server connection
s.close()

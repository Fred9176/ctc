<?php


session_start();

require_once('./inc/CTC.php');
require_once('./inc/Rocrail.php');


/* INITIALIZATION */
$CTC = new CTC();


/* ACTIONS */
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
switch($action){
    
    /* EXECURE SWITCH COMMAND (AJAX) */
    case 'AjaxSwitchCommand':
        $CTC->sendSwitchCommand($_REQUEST['switchID'], $_REQUEST['direction']);
        break;
    
    /* RETRIEVE SWITCHES STATES (AJAX) */
    case 'AjaxPanelSwitchListState' :
        echo $CTC->getSwitchesCurrentState();
        break;
        
    /* DISPLAY PANEL */
    default : 
        $CTC->displayPanel();
}

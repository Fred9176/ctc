<?php

/*
 * This class is used to interface CTC with Rocrail
 */

class CS {

    private $address = '192.168.1.40';
    private $port = 8051;
    private $timeout = 30;
    private $connCS = false;

    /**
     * CLASS INIT : RETRIEVE IP AND PORT
     * 
     * @param array $params array of parameters to initialize the class
     */
    function __construct($params = array()) {

        // Retrieve parameters if we don't want to use default ones
        if (isset($params['ip'])) {
            $this->ip = $params['ip'];
        }
        if (isset($params['port'])) {
            $this->port = $params['port'];
        }
        if (isset($params['timeout'])) {
            $this->timeout = $params['timeout'];
        }

        // Open Connexion
        $this->openSocket();
    }


    /**
     * SEND SWITCH COMMAND
     * 
     * @param string $switchID ID of the switch to command
     * @param string $cmd Command to send
     */
    public function sendSwitchCommand($switchID, $cmd) {
        $msg = '<sw id="' . $switchID . '" cmd="' . $cmd . '"/>';
        $this->send('sw', $msg);
    }

    /*
     * GET SWITCHES PARAMETERS
     * 
     * @param array $params List of parameters to retrieve for each switch
     * 
     * @return array $switches List of requested switches
     */

    public function getSwitches($params = array('type', 'state')) {

        // Get switch list from Rocrail
        $msg = '<model cmd="swlist" />';
        $swlist = $this->get('model', $msg, 'swlist');

        // Retrieve requested switches
        if (!$swlist) {
            $switches = false;
        } else {
            $swlistXml = new SimpleXMLElement($swlist);

            $switches = array();
            foreach ($swlistXml as $sw) {
                $switchID = (string) $sw['id'];

                foreach ($params as $param) {
                    $switches[$switchID][$param] = (string) $sw[$param];
                }
            }
        }
        return $switches;
    }
    
    
    /*
     * GET SWITCH DIRECTION ACCORDING TO CS TYPE AND STATE
     * 
     * @param string $type Type of switch (left, right, twoway)
     * @param string $state Current state (straigt, turnout)
     * 
     * @result string $direction Direction to display (left, right)
     */

    public function getSwitchDirection($type, $state) {
        switch ($type) {
            // right turnout
            case 'right' :
                $direction = $state == 'straight' ? 'left' : 'right';
                break;

            // Other types (left, twoway)
            default :

                $direction = $state == 'straight' ? 'right' : 'left';
                break;
        }

        return $direction;
    }

    /*
     * GET SWITCH STATE
     * 
     * @param string $type Type of switch (left, right, twoway)
     * @param string $direction Current state (left, right)
     * 
     * @result string $state State to set (straight, turnout)
     */

    public function getSwitchState($type, $direction) {
        switch ($type) {
            // right turnout
            case 'right' :
                $state = $direction == 'left' ? 'straight' : 'turnout';
                break;

            // Other types (left, twoway)
            default :

                $state = $direction == 'left' ? 'turnout' : 'straight';
                break;
        }

        return $state;
    }
    


    /**
     * OPEN A CONNECTION WITH ROCRAIL
     */
    private function openSocket() {
        $this->connCS = @fsockopen($this->address, $this->port, $errno, $errstr, $this->timeout);
    }
    

    /**
     * CREATE XML MESSAGE TO SEND TO ROCRAIL
     *
     * @param string $xmlType Node of the client service to work with
     * @param string $xmlMsg XML message to send
     * 
     * @return string $msg Formatted message with header
     */
    private function createMsg($xmlType, $xmlMsg) {
        $msg = sprintf('<xmlh><xml size="%d" name="%s"/></xmlh>%s', strlen($xmlMsg), $xmlType, $xmlMsg);
        return $msg;
    }

    /**
     * SEND MESSAGE TO ROCRAIL
     *
     * @param string $type Node of the client service to work with
     * @param string $msg XML message to send
     * 
     * @return bool Command correctly executed     
     */
    private function send($type, $msg) {

        if ($this->connCS) {

            $msg = $this->createMsg($type, $msg);
            $return = fwrite($this->connCS, $msg);

            // Return ok if all characters are sent
            return ($return == strlen($msg));
        } else {
            return false;
        }
    }

    /**
     * GET MESSAGE FROM ROCRAIL
     *
     * @param string $type Node of the client service to work with
     * @param string $msg XML message to send
     * @param string $returnTag Node returned if different from $type to detect start and end of response
     * 
     * @return string $response Response from Rocrail server
     */
    private function get($type, $msg, $returnTag = '') {

        // Prepare return tag
        if ($returnTag == '') {
            $returnTag = $type;
        }
        $startTag = '<' . $returnTag . '>';
        $endTag = '</' . $returnTag . '>';


        if ($this->send($type, $msg)) {

            // Start getting response
            $return = fgets($this->connCS, 128);

            // Read resonse until end tag is detected
            while (strpos($return, $endTag) === false) {
                $return .= fgets($this->connCS, 128);
            }

            // Remove response header
            $xmlStart = strpos($return, $startTag);
            $return = substr($return, $xmlStart);

            return $return;
        }

        // Failed to send request
        else {
            return false;
        }
    }

}
